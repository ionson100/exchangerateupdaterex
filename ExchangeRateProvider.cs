﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ExchangeRateUpdater
{
    public class ExchangeRateProvider
    {
        private const string Url = "http://www.norges-bank.no/WebDAV/stat/valutakurser/sdv/valuta_dag.sdv";
        /// <summary>
        /// Should return exchange rates among the specified currencies that are defined by the source. But only those defined
        /// by the source, do not return calculated exchange rates. E.g. if the source contains "EUR/USD" but not "USD/EUR",
        /// do not return exchange rate "USD/EUR" with value calculated as 1 / "EUR/USD". If the source does not provide
        /// some of the currencies, ignore them.
        /// </summary>
        public async Task<IEnumerable<ExchangeRate>> GetExchangeRates(IEnumerable<Currency> currencies)
        {
            var exchangeRates = new List<ExchangeRate>();
            var dataTable = new DataTable();
            using (var client = new WebClient())
            {
                /*get csv from fishing country*/
                var data = await client.OpenReadTaskAsync(new Uri(Url, UriKind.Absolute));
                /*make data table*/
                using (var reader = new StreamReader(data))
                {
                    var isFirstRow = true;
                    while (!reader.EndOfStream)
                    {
                        var readLine = reader.ReadLine();
                        if (readLine == null) continue;
                        if (isFirstRow)
                        {
                            isFirstRow = false;
                            dataTable.Columns.AddRange(readLine.Split(';').Select(s => new DataColumn(s)).ToArray());
                        }
                        else
                        {
                            dataTable.Rows.Add(readLine.Split(';'));
                        }
                    }
                }
               
            }
            foreach (var currency in currencies)
            {
                for (var i = 0; i < dataTable.Columns.Count; i++)
                {

                    /*prepare target currency*/
                    if (dataTable.Columns[i].ColumnName.Contains(currency.Code))
                    {
                        //var row =dataTable.AsEnumerable()  .FirstOrDefault(  dataRow => dataRow.Field<string>("Date") == new DateTime().ToString("DD-MMM-YY"));
                        var row = dataTable.Rows[dataTable.Rows.Count - 2];
                        var ss = row[i].ToString();
                        if (ss == "ND"||ss=="NA")
                            ss = "-1";
                        var d = decimal.Parse(ss);
                        if (dataTable.Columns[i].ColumnName.Contains("100 "))
                        {
                            d = d / 100;
                        }
                        exchangeRates.Add(new ExchangeRate(new Currency("NOK"), currency, d));
                      
                    }
                }
            }
            return exchangeRates;
        }
    }
}
